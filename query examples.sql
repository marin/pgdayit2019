-- create example tables
DROP TABLE IF EXISTS a CASCADE;

CREATE TABLE a(
    id BIGSERIAL NOT NULL PRIMARY KEY,
    type_id SMALLINT
);

DROP TABLE IF EXISTS b CASCADE;

CREATE TABLE b(
    id SERIAL NOT NULL PRIMARY KEY,
    type_id SMALLINT,
    a_id INT REFERENCES a(id),
    some_data CHAR(10)
);

CREATE INDEX b_a ON b(a_id);

-- populate tables with data
-- use setseed to have repeatable results
SELECT setseed(0.314159);

-- add 1M rows to a
INSERT INTO a(type_id)
SELECT (random()*10+1)::int FROM generate_series(1, 1000000) i;

-- add 2M rows to b
INSERT INTO b(type_id, a_id, some_data)
SELECT (random()*10+1)::int, i%1000000+1,  i::CHAR(10) FROM generate_series(1, 2000000) i;

-- get fresh statistics
ANALYSE a;
ANALYSE b;

-- basic example
EXPLAIN ANALYSE
SELECT a.*
FROM a
WHERE a.id <= 10;

/*
Index Scan using a_pkey on a  (cost=0.42..8.58 rows=9 width=10) (actual time=0.010..0.016 rows=10 loops=1)
  Index Cond: (id <= 10)
Total runtime: 0.051 ms
*/

-- basic join with index
EXPLAIN ANALYSE
SELECT a.*
FROM a
INNER JOIN b ON a.id = b.a_id
WHERE a.id <= 10;

/*
Nested Loop  (cost=0.85..112.04 rows=18 width=10) (actual time=0.028..0.133 rows=20 loops=1)
  ->  Index Scan using a_pkey on a  (cost=0.42..8.58 rows=9 width=10) (actual time=0.009..0.016 rows=10 loops=1)
        Index Cond: (id <= 10)
  ->  Index Only Scan using b_a on b  (cost=0.43..11.48 rows=2 width=4) (actual time=0.007..0.009 rows=2 loops=10)
        Index Cond: (a_id = a.id)
        Heap Fetches: 20
Total runtime: 0.183 ms
*/

-- without the b_a index 

/*
Hash Join  (cost=8.70..40247.88 rows=18 width=10) (actual time=0.055..475.437 rows=20 loops=1)
  Hash Cond: (b.a_id = a.id)
  ->  Seq Scan on b  (cost=0.00..32739.00 rows=2000000 width=4) (actual time=0.015..223.046 rows=2000000 loops=1)
  ->  Hash  (cost=8.58..8.58 rows=9 width=10) (actual time=0.023..0.023 rows=10 loops=1)
        Buckets: 1024  Batches: 1  Memory Usage: 1kB
        ->  Index Scan using a_pkey on a  (cost=0.42..8.58 rows=9 width=10) (actual time=0.008..0.011 rows=10 loops=1)
              Index Cond: (id <= 10)
Total runtime: 475.498 ms
*/

-- generic estimate
EXPLAIN ANALYSE
SELECT a.*
FROM a
WHERE a.id <= (SELECT 10);

/*
Index Scan using a_pkey on a  (cost=0.43..11298.76 rows=333333 width=10) (actual time=0.030..0.037 rows=10 loops=1)
  Index Cond: (id <= $0)
  InitPlan 1 (returns $0)
    ->  Result  (cost=0.00..0.01 rows=1 width=0) (actual time=0.002..0.002 rows=1 loops=1)
Total runtime: 0.082 ms
*/

-- basic join with generic estimate
EXPLAIN ANALYSE
SELECT a.*
FROM a
INNER JOIN b ON a.id = b.a_id
WHERE a.id <= (SELECT 10);

/*
Hash Join  (cost=15465.42..72192.03 rows=648761 width=10) (actual time=0.248..501.330 rows=20 loops=1)
  Hash Cond: (b.a_id = a.id)
  InitPlan 1 (returns $0)
    ->  Result  (cost=0.00..0.01 rows=1 width=0) (actual time=0.002..0.002 rows=1 loops=1)
  ->  Seq Scan on b  (cost=0.00..32739.00 rows=2000000 width=4) (actual time=0.015..223.640 rows=2000000 loops=1)
  ->  Hash  (cost=11298.75..11298.75 rows=333333 width=10) (actual time=0.042..0.042 rows=10 loops=1)
        Buckets: 65536  Batches: 1  Memory Usage: 1kB
        ->  Index Scan using a_pkey on a  (cost=0.42..11298.75 rows=333333 width=10) (actual time=0.025..0.031 rows=10 loops=1)
              Index Cond: (id <= $0)
Total runtime: 501.394 ms
*/

-- basic example with range hint
EXPLAIN ANALYSE
SELECT a.*
FROM a
WHERE a.id <= (SELECT 10)
AND a.id > 0;

/*
Index Scan using a_pkey on a  (cost=0.43..187.44 rows=5000 width=10) (actual time=0.024..0.030 rows=10 loops=1)
  Index Cond: ((id <= $0) AND (id > 0))
  InitPlan 1 (returns $0)
    ->  Result  (cost=0.00..0.01 rows=1 width=0) (actual time=0.001..0.002 rows=1 loops=1)
Total runtime: 0.073 ms
*/

-- basic join with with range hint
EXPLAIN ANALYSE
SELECT a.*
FROM a
INNER JOIN b ON a.id = b.a_id
WHERE a.id <= (SELECT 10)
AND a.id > 0;

/*
Nested Loop  (cost=0.86..42115.21 rows=9731 width=10) (actual time=0.039..0.141 rows=20 loops=1)
  InitPlan 1 (returns $0)
    ->  Result  (cost=0.00..0.01 rows=1 width=0) (actual time=0.002..0.002 rows=1 loops=1)
  ->  Index Scan using a_pkey on a  (cost=0.42..187.43 rows=5000 width=10) (actual time=0.023..0.027 rows=10 loops=1)
        Index Cond: ((id <= $0) AND (id > 0))
  ->  Index Only Scan using b_a on b  (cost=0.43..8.37 rows=2 width=4) (actual time=0.007..0.009 rows=2 loops=10)
        Index Cond: (a_id = a.id)
        Heap Fetches: 20
Total runtime: 0.203 ms
*/

-- basic join with additional filter
EXPLAIN ANALYSE
SELECT a.*
FROM a
INNER JOIN b ON a.id = b.a_id
WHERE a.id <= 10
AND a.type_id <= 1;

/*
Nested Loop  (cost=0.85..20.10 rows=2 width=10) (actual time=0.022..0.022 rows=0 loops=1)
  ->  Index Scan using a_pkey on a  (cost=0.42..8.61 rows=1 width=10) (actual time=0.021..0.021 rows=0 loops=1)
        Index Cond: (id <= 10)
        Filter: (type_id <= 1)
        Rows Removed by Filter: 10
  ->  Index Only Scan using b_a on b  (cost=0.43..11.48 rows=2 width=4) (never executed)
        Index Cond: (a_id = a.id)
        Heap Fetches: 0
Total runtime: 0.072 ms
*/

-- basic join with additional filter using generic estimate
EXPLAIN ANALYSE
SELECT a.*
FROM a
INNER JOIN b ON a.id = b.a_id
WHERE a.id <= (SELECT 10)
AND a.type_id <= 1;

/*
Hash Join  (cost=12337.09..70395.28 rows=31919 width=10) (actual time=0.054..0.054 rows=0 loops=1)
  Hash Cond: (b.a_id = a.id)
  InitPlan 1 (returns $0)
    ->  Result  (cost=0.00..0.01 rows=1 width=0) (actual time=0.001..0.001 rows=1 loops=1)
  ->  Seq Scan on b  (cost=0.00..32739.00 rows=2000000 width=4) (actual time=0.016..0.016 rows=1 loops=1)
  ->  Hash  (cost=12132.08..12132.08 rows=16400 width=10) (actual time=0.026..0.026 rows=0 loops=1)
        Buckets: 2048  Batches: 1  Memory Usage: 0kB
        ->  Index Scan using a_pkey on a  (cost=0.42..12132.08 rows=16400 width=10) (actual time=0.026..0.026 rows=0 loops=1)
              Index Cond: (id <= $0)
              Filter: (type_id <= 1)
              Rows Removed by Filter: 10
Total runtime: 0.116 ms
*/

-- basic join with additional filter using range hint
EXPLAIN ANALYSE
SELECT a.*
FROM a
INNER JOIN b ON a.id = b.a_id
WHERE a.id <= (SELECT 10)
AND a.type_id <= 1
AND a.id > 0;

/*
Nested Loop  (cost=0.86..2978.79 rows=479 width=10) (actual time=0.034..0.034 rows=0 loops=1)
  InitPlan 1 (returns $0)
    ->  Result  (cost=0.00..0.01 rows=1 width=0) (actual time=0.002..0.003 rows=1 loops=1)
  ->  Index Scan using a_pkey on a  (cost=0.42..199.93 rows=246 width=10) (actual time=0.034..0.034 rows=0 loops=1)
        Index Cond: ((id <= $0) AND (id > 0))
        Filter: (type_id <= 1)
        Rows Removed by Filter: 10
  ->  Index Only Scan using b_a on b  (cost=0.43..11.28 rows=2 width=4) (never executed)
        Index Cond: (a_id = a.id)
        Heap Fetches: 0
Total runtime: 0.092 ms
*/


-- basic join with additional filter on other table
EXPLAIN ANALYSE
SELECT a.*
FROM a
INNER JOIN b ON a.id = b.a_id
WHERE a.id <= 10
AND b.type_id <= 1;

/*
Nested Loop  (cost=0.85..112.00 rows=1 width=10) (actual time=0.064..0.126 rows=2 loops=1)
  ->  Index Scan using a_pkey on a  (cost=0.42..8.58 rows=9 width=10) (actual time=0.010..0.017 rows=10 loops=1)
        Index Cond: (id <= 10)
  ->  Index Scan using b_a on b  (cost=0.43..11.48 rows=1 width=4) (actual time=0.009..0.010 rows=0 loops=10)
        Index Cond: (a_id = a.id)
        Filter: (type_id <= 1)
        Rows Removed by Filter: 2
Total runtime: 0.178 ms
*/

-- basic join with additional filter on other table using generic estimate
EXPLAIN ANALYSE
SELECT a.*
FROM a
INNER JOIN b ON a.id = b.a_id
WHERE a.id <= (SELECT 10)
AND b.type_id <= 1;

/*
Hash Join  (cost=15465.42..54424.59 rows=33000 width=10) (actual time=0.174..324.219 rows=2 loops=1)
  Hash Cond: (b.a_id = a.id)
  InitPlan 1 (returns $0)
    ->  Result  (cost=0.00..0.01 rows=1 width=0) (actual time=0.001..0.002 rows=1 loops=1)
  ->  Seq Scan on b  (cost=0.00..37739.00 rows=101733 width=4) (actual time=0.019..305.319 rows=100332 loops=1)
        Filter: (type_id <= 1)
        Rows Removed by Filter: 1899668
  ->  Hash  (cost=11298.75..11298.75 rows=333333 width=10) (actual time=0.034..0.034 rows=10 loops=1)
        Buckets: 65536  Batches: 1  Memory Usage: 1kB
        ->  Index Scan using a_pkey on a  (cost=0.42..11298.75 rows=333333 width=10) (actual time=0.018..0.026 rows=10 loops=1)
              Index Cond: (id <= $0)
Total runtime: 324.282 ms
*/

-- basic join with additional filter on other table using range hint
EXPLAIN ANALYSE
SELECT a.*
FROM a
INNER JOIN b ON a.id = b.a_id
WHERE a.id <= (SELECT 10)
AND b.type_id <= 1
AND a.id > 0;

/*
Hash Join  (cost=249.94..38884.05 rows=495 width=10) (actual time=0.070..299.836 rows=2 loops=1)
  Hash Cond: (b.a_id = a.id)
  InitPlan 1 (returns $0)
    ->  Result  (cost=0.00..0.01 rows=1 width=0) (actual time=0.001..0.001 rows=1 loops=1)
  ->  Seq Scan on b  (cost=0.00..37739.00 rows=101733 width=4) (actual time=0.020..286.537 rows=100332 loops=1)
        Filter: (type_id <= 1)
        Rows Removed by Filter: 1899668
  ->  Hash  (cost=187.43..187.43 rows=5000 width=10) (actual time=0.035..0.035 rows=10 loops=1)
        Buckets: 1024  Batches: 1  Memory Usage: 1kB
        ->  Index Scan using a_pkey on a  (cost=0.42..187.43 rows=5000 width=10) (actual time=0.021..0.024 rows=10 loops=1)
              Index Cond: ((id <= $0) AND (id > 0))
Total runtime: 299.906 ms
*/

-- subselect with additional filter on other table using range hint
EXPLAIN ANALYSE
SELECT ir.id, ir.type_id
FROM (
	SELECT a.*, b.id AS b_id, b.type_id AS b_type_id
	FROM a
	INNER JOIN b ON a.id = b.a_id
	WHERE a.id <= (SELECT 10)
	AND a.id > 0
) AS ir
WHERE ir.b_type_id <= 1;

/*
Hash Join  (cost=249.94..38884.05 rows=495 width=10) (actual time=0.069..312.706 rows=2 loops=1)
  Hash Cond: (b.a_id = a.id)
  InitPlan 1 (returns $0)
    ->  Result  (cost=0.00..0.01 rows=1 width=0) (actual time=0.001..0.001 rows=1 loops=1)
  ->  Seq Scan on b  (cost=0.00..37739.00 rows=101733 width=4) (actual time=0.020..299.735 rows=100332 loops=1)
        Filter: (type_id <= 1)
        Rows Removed by Filter: 1899668
  ->  Hash  (cost=187.43..187.43 rows=5000 width=10) (actual time=0.035..0.035 rows=10 loops=1)
        Buckets: 1024  Batches: 1  Memory Usage: 1kB
        ->  Index Scan using a_pkey on a  (cost=0.42..187.43 rows=5000 width=10) (actual time=0.021..0.031 rows=10 loops=1)
              Index Cond: ((id <= $0) AND (id > 0))
Total runtime: 312.776 ms
*/

-- using CTE to block optimisation
EXPLAIN ANALYSE
WITH interesting_rows AS(
	SELECT a.*, b.type_id AS b_type_id
	FROM a
	INNER JOIN b ON a.id = b.a_id
	WHERE a.id <= (SELECT 10)
	AND a.id >0
)
SELECT id, type_id FROM interesting_rows
WHERE b_type_id = 1;

/*
CTE Scan on interesting_rows  (cost=42115.21..42334.16 rows=49 width=10) (actual time=0.082..0.157 rows=2 loops=1)
  Filter: (b_type_id = 1)
  Rows Removed by Filter: 18
  CTE interesting_rows
    ->  Nested Loop  (cost=0.86..42115.21 rows=9731 width=16) (actual time=0.033..0.127 rows=20 loops=1)
          InitPlan 1 (returns $0)
            ->  Result  (cost=0.00..0.01 rows=1 width=0) (actual time=0.001..0.001 rows=1 loops=1)
          ->  Index Scan using a_pkey on a  (cost=0.42..187.43 rows=5000 width=10) (actual time=0.022..0.028 rows=10 loops=1)
                Index Cond: ((id <= $0) AND (id > 0))
          ->  Index Scan using b_a on b  (cost=0.43..8.37 rows=2 width=10) (actual time=0.006..0.008 rows=2 loops=10)
                Index Cond: (a_id = a.id)
Total runtime: 0.220 ms
*/

-- subselect with additional filter on other table with offset 0
EXPLAIN ANALYSE
SELECT id, type_id
FROM (
	SELECT a.*, b.type_id AS b_type_id
	FROM a
	INNER JOIN b ON a.id = b.a_id
	WHERE a.id <= (SELECT 10)
	AND a.id > 0
	OFFSET 0
) AS ir
WHERE ir.b_type_id <= 1;

/*
Subquery Scan on ir  (cost=0.86..42236.85 rows=495 width=10) (actual time=0.119..0.191 rows=2 loops=1)
  Filter: (ir.b_type_id <= 1)
  Rows Removed by Filter: 18
  ->  Nested Loop  (cost=0.86..42115.21 rows=9731 width=12) (actual time=0.074..0.179 rows=20 loops=1)
        InitPlan 1 (returns $0)
          ->  Result  (cost=0.00..0.01 rows=1 width=0) (actual time=0.001..0.001 rows=1 loops=1)
        ->  Index Scan using a_pkey on a  (cost=0.42..187.43 rows=5000 width=10) (actual time=0.058..0.067 rows=10 loops=1)
              Index Cond: ((id <= $0) AND (id > 0))
        ->  Index Scan using b_a on b  (cost=0.43..8.37 rows=2 width=6) (actual time=0.007..0.009 rows=2 loops=10)
              Index Cond: (a_id = a.id)
Total runtime: 0.257 ms
*/

-- just create an index
CREATE INDEX b_type_a ON b(a_id, type_id);

EXPLAIN ANALYSE
SELECT a.*
FROM a
INNER JOIN b ON a.id = b.a_id
WHERE a.id <= (SELECT 10)
AND a.id > 0
AND b.type_id <= 1;

/*
Nested Loop  (cost=0.86..32947.44 rows=495 width=10) (actual time=0.064..0.111 rows=2 loops=1)
  InitPlan 1 (returns $0)
    ->  Result  (cost=0.00..0.01 rows=1 width=0) (actual time=0.001..0.001 rows=1 loops=1)
  ->  Index Scan using a_pkey on a  (cost=0.42..187.43 rows=5000 width=10) (actual time=0.024..0.035 rows=10 loops=1)
        Index Cond: ((id <= $0) AND (id > 0))
  ->  Index Only Scan using b_type_a on b  (cost=0.43..6.54 rows=1 width=4) (actual time=0.007..0.007 rows=0 loops=10)
        Index Cond: ((a_id = a.id) AND (type_id <= 1))
        Heap Fetches: 2
Total runtime: 0.173 ms
*/

-- join order
DROP TABLE IF EXISTS c CASCADE;

CREATE TABLE c(
    id BIGSERIAL NOT NULL PRIMARY KEY,
    b_id BIGINT REFERENCES b(id)
);

DROP TABLE IF EXISTS type_data CASCADE;

CREATE TABLE type_data(
    id SMALLINT,
    some_data CHAR(100)
);

INSERT INTO c(b_id)
SELECT (i+123456)%200000+1 FROM generate_series(1, 2000000) i;

INSERT INTO type_data(id, some_data)
SELECT i, 'Data ' || i FROM generate_series(1,11) i;

ANALYZE c;
ANALYSE type_data;

EXPLAIN ANALYZE
SELECT a.id
FROM c
INNER JOIN b ON b.id = c.b_id
INNER JOIN type_data b_data ON b_data.id = b.type_id
INNER JOIN a ON a.id = b.a_id
INNER JOIN type_data a_data ON a_data.id = a.type_id
WHERE a.id < 10;
/*
Nested Loop  (cost=112.13..38431.53 rows=18 width=8) (actual time=40.456..451.546 rows=80 loops=1)
  Join Filter: (a.type_id = a_data.id)
  Rows Removed by Join Filter: 800
  ->  Nested Loop  (cost=112.13..38427.42 rows=18 width=10) (actual time=40.447..451.352 rows=80 loops=1)
        Join Filter: (b.type_id = b_data.id)
        Rows Removed by Join Filter: 800
        ->  Hash Join  (cost=112.13..38423.31 rows=18 width=12) (actual time=40.434..451.136 rows=80 loops=1)
              Hash Cond: (c.b_id = b.id)
              ->  Seq Scan on c  (cost=0.00..30811.00 rows=2000000 width=8) (actual time=0.017..214.859 rows=2000000 loops=1)
              ->  Hash  (cost=111.91..111.91 rows=18 width=16) (actual time=0.141..0.141 rows=18 loops=1)
                    Buckets: 1024  Batches: 1  Memory Usage: 1kB
                    ->  Nested Loop  (cost=0.85..111.91 rows=18 width=16) (actual time=0.025..0.126 rows=18 loops=1)
                          ->  Index Scan using a_pkey on a  (cost=0.42..8.58 rows=9 width=10) (actual time=0.009..0.017 rows=9 loops=1)
                                Index Cond: (id < 10)
                          ->  Index Scan using b_a on b  (cost=0.43..11.46 rows=2 width=10) (actual time=0.007..0.009 rows=2 loops=9)
                                Index Cond: (a_id = a.id)
        ->  Materialize  (cost=0.00..1.17 rows=11 width=2) (actual time=0.000..0.001 rows=11 loops=80)
              ->  Seq Scan on type_data b_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.004..0.005 rows=11 loops=1)
  ->  Materialize  (cost=0.00..1.17 rows=11 width=2) (actual time=0.000..0.001 rows=11 loops=80)
        ->  Seq Scan on type_data a_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.001..0.003 rows=11 loops=1)
Total runtime: 451.657 ms
*/

EXPLAIN ANALYZE
SELECT a.id
FROM a
INNER JOIN type_data a_data ON a_data.id = a.type_id
INNER JOIN b ON b.a_id = a.id
INNER JOIN type_data b_data ON b_data.id = b.type_id
INNER JOIN c ON c.b_id = b.id
WHERE a.id < 10;
/*
Nested Loop  (cost=112.13..38431.53 rows=18 width=8) (actual time=38.381..454.297 rows=80 loops=1)
  Join Filter: (b.type_id = b_data.id)
  Rows Removed by Join Filter: 800
  ->  Nested Loop  (cost=112.13..38427.42 rows=18 width=10) (actual time=38.372..454.088 rows=80 loops=1)
        Join Filter: (a.type_id = a_data.id)
        Rows Removed by Join Filter: 800
        ->  Hash Join  (cost=112.13..38423.31 rows=18 width=12) (actual time=38.359..453.863 rows=80 loops=1)
              Hash Cond: (c.b_id = b.id)
              ->  Seq Scan on c  (cost=0.00..30811.00 rows=2000000 width=8) (actual time=0.017..215.447 rows=2000000 loops=1)
              ->  Hash  (cost=111.91..111.91 rows=18 width=16) (actual time=0.139..0.139 rows=18 loops=1)
                    Buckets: 1024  Batches: 1  Memory Usage: 1kB
                    ->  Nested Loop  (cost=0.85..111.91 rows=18 width=16) (actual time=0.024..0.121 rows=18 loops=1)
                          ->  Index Scan using a_pkey on a  (cost=0.42..8.58 rows=9 width=10) (actual time=0.009..0.015 rows=9 loops=1)
                                Index Cond: (id < 10)
                          ->  Index Scan using b_a on b  (cost=0.43..11.46 rows=2 width=10) (actual time=0.007..0.009 rows=2 loops=9)
                                Index Cond: (a_id = a.id)
        ->  Materialize  (cost=0.00..1.17 rows=11 width=2) (actual time=0.000..0.001 rows=11 loops=80)
              ->  Seq Scan on type_data a_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.004..0.004 rows=11 loops=1)
  ->  Materialize  (cost=0.00..1.17 rows=11 width=2) (actual time=0.000..0.001 rows=11 loops=80)
        ->  Seq Scan on type_data b_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.001..0.003 rows=11 loops=1)
Total runtime: 454.408 ms
*/

SET join_collapse_limit = 1;

EXPLAIN ANALYZE
SELECT a.id
FROM c
INNER JOIN b ON b.id = c.b_id
INNER JOIN type_data b_data ON b_data.id = b.type_id
INNER JOIN a ON a.id = b.a_id
INNER JOIN type_data a_data ON a_data.id = a.type_id
WHERE a.id < 10;
/*
Hash Join  (cost=57750.19..168561.62 rows=18 width=8) (actual time=1102.351..5176.139 rows=80 loops=1)
  Hash Cond: (a.type_id = a_data.id)
  ->  Hash Join  (cost=57748.94..168560.12 rows=18 width=10) (actual time=1102.319..5176.071 rows=80 loops=1)
        Hash Cond: (b.a_id = a.id)
        ->  Hash Join  (cost=57740.25..161051.25 rows=2000000 width=4) (actual time=934.911..4951.964 rows=2000000 loops=1)
              Hash Cond: (b.type_id = b_data.id)
              ->  Hash Join  (cost=57739.00..133550.00 rows=2000000 width=6) (actual time=934.889..4331.782 rows=2000000 loops=1)
                    Hash Cond: (c.b_id = b.id)
                    ->  Seq Scan on c  (cost=0.00..30811.00 rows=2000000 width=8) (actual time=0.014..270.451 rows=2000000 loops=1)
                    ->  Hash  (cost=32739.00..32739.00 rows=2000000 width=10) (actual time=934.371..934.371 rows=2000000 loops=1)
                          Buckets: 262144  Batches: 1  Memory Usage: 85938kB
                          ->  Seq Scan on b  (cost=0.00..32739.00 rows=2000000 width=10) (actual time=0.018..389.222 rows=2000000 loops=1)
              ->  Hash  (cost=1.11..1.11 rows=11 width=2) (actual time=0.012..0.012 rows=11 loops=1)
                    Buckets: 1024  Batches: 1  Memory Usage: 1kB
                    ->  Seq Scan on type_data b_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.003..0.007 rows=11 loops=1)
        ->  Hash  (cost=8.58..8.58 rows=9 width=10) (actual time=0.020..0.020 rows=9 loops=1)
              Buckets: 1024  Batches: 1  Memory Usage: 1kB
              ->  Index Scan using a_pkey on a  (cost=0.42..8.58 rows=9 width=10) (actual time=0.009..0.013 rows=9 loops=1)
                    Index Cond: (id < 10)
  ->  Hash  (cost=1.11..1.11 rows=11 width=2) (actual time=0.022..0.022 rows=11 loops=1)
        Buckets: 1024  Batches: 1  Memory Usage: 1kB
        ->  Seq Scan on type_data a_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.009..0.014 rows=11 loops=1)
Total runtime: 5183.119 ms
*/

EXPLAIN ANALYZE
SELECT a.id
FROM a
INNER JOIN type_data a_data ON a_data.id = a.type_id
INNER JOIN b ON b.a_id = a.id
INNER JOIN type_data b_data ON b_data.id = b.type_id
INNER JOIN c ON c.b_id = b.id
WHERE a.id < 10;
/*
Hash Join  (cost=115.00..38426.18 rows=18 width=8) (actual time=36.031..447.317 rows=80 loops=1)
  Hash Cond: (c.b_id = b.id)
  ->  Seq Scan on c  (cost=0.00..30811.00 rows=2000000 width=8) (actual time=0.017..210.918 rows=2000000 loops=1)
  ->  Hash  (cost=114.77..114.77 rows=18 width=12) (actual time=0.212..0.212 rows=18 loops=1)
        Buckets: 1024  Batches: 1  Memory Usage: 1kB
        ->  Hash Join  (cost=3.35..114.77 rows=18 width=12) (actual time=0.075..0.197 rows=18 loops=1)
              Hash Cond: (b.type_id = b_data.id)
              ->  Nested Loop  (cost=2.10..113.28 rows=18 width=14) (actual time=0.048..0.152 rows=18 loops=1)
                    ->  Hash Join  (cost=1.67..9.95 rows=9 width=8) (actual time=0.032..0.046 rows=9 loops=1)
                          Hash Cond: (a.type_id = a_data.id)
                          ->  Index Scan using a_pkey on a  (cost=0.42..8.58 rows=9 width=10) (actual time=0.009..0.016 rows=9 loops=1)
                                Index Cond: (id < 10)
                          ->  Hash  (cost=1.11..1.11 rows=11 width=2) (actual time=0.012..0.012 rows=11 loops=1)
                                Buckets: 1024  Batches: 1  Memory Usage: 1kB
                                ->  Seq Scan on type_data a_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.003..0.009 rows=11 loops=1)
                    ->  Index Scan using b_a on b  (cost=0.43..11.46 rows=2 width=10) (actual time=0.007..0.009 rows=2 loops=9)
                          Index Cond: (a_id = a.id)
              ->  Hash  (cost=1.11..1.11 rows=11 width=2) (actual time=0.018..0.018 rows=11 loops=1)
                    Buckets: 1024  Batches: 1  Memory Usage: 1kB
                    ->  Seq Scan on type_data b_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.005..0.010 rows=11 loops=1)
Total runtime: 447.425 ms

*/

/*
-----------------------------------------------
Parallel sequential scan
----------------------------------------------- 
*/

SET max_parallel_workers_per_gather = 0;
SET join_collapse_limit = 1;

EXPLAIN ANALYZE
SELECT a.id
FROM c
INNER JOIN b ON b.id = c.b_id
INNER JOIN type_data b_data ON b_data.id = b.type_id
INNER JOIN a ON a.id = b.a_id
INNER JOIN type_data a_data ON a_data.id = a.type_id
WHERE a.id < 10;
/*
Hash Join  (cost=67516.19..161719.47 rows=18 width=8) (actual time=2022.431..3346.324 rows=80 loops=1)
  Hash Cond: (a.type_id = a_data.id)
  ->  Hash Join  (cost=67514.94..161717.98 rows=18 width=10) (actual time=2022.414..3346.248 rows=80 loops=1)
        Hash Cond: (b.a_id = a.id)
        ->  Hash Join  (cost=67506.25..156459.26 rows=2000000 width=4) (actual time=890.963..3131.228 rows=2000000 loops=1)
              Hash Cond: (b.type_id = b_data.id)
              ->  Hash Join  (cost=67505.00..128958.01 rows=2000000 width=6) (actual time=890.950..2622.205 rows=2000000 loops=1)
                    Hash Cond: (c.b_id = b.id)
                    ->  Seq Scan on c  (cost=0.00..30811.00 rows=2000000 width=8) (actual time=0.021..313.567 rows=2000000 loops=1)
                    ->  Hash  (cost=32739.00..32739.00 rows=2000000 width=10) (actual time=890.654..890.654 rows=2000000 loops=1)
                          Buckets: 524288  Batches: 8  Memory Usage: 14853kB
                          ->  Seq Scan on b  (cost=0.00..32739.00 rows=2000000 width=10) (actual time=0.015..400.576 rows=2000000 loops=1)
              ->  Hash  (cost=1.11..1.11 rows=11 width=2) (actual time=0.010..0.010 rows=11 loops=1)
                    Buckets: 1024  Batches: 1  Memory Usage: 9kB
                    ->  Seq Scan on type_data b_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.002..0.005 rows=11 loops=1)
        ->  Hash  (cost=8.58..8.58 rows=9 width=10) (actual time=0.013..0.014 rows=9 loops=1)
              Buckets: 1024  Batches: 1  Memory Usage: 9kB
              ->  Index Only Scan using cover_a on a  (cost=0.42..8.58 rows=9 width=10) (actual time=0.005..0.009 rows=9 loops=1)
                    Index Cond: (id < 10)
                    Heap Fetches: 9
  ->  Hash  (cost=1.11..1.11 rows=11 width=2) (actual time=0.013..0.013 rows=11 loops=1)
        Buckets: 1024  Batches: 1  Memory Usage: 9kB
        ->  Seq Scan on type_data a_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.005..0.008 rows=11 loops=1)
Planning Time: 0.642 ms
Execution Time: 3346.453 ms
*/

SET max_parallel_workers_per_gather = 1;
SET join_collapse_limit = 1;

EXPLAIN ANALYZE
SELECT a.id
FROM c
INNER JOIN b ON b.id = c.b_id
INNER JOIN type_data b_data ON b_data.id = b.type_id
INNER JOIN a ON a.id = b.a_id
INNER JOIN type_data a_data ON a_data.id = a.type_id
WHERE a.id < 10;
/*
Gather  (cost=45965.78..105833.41 rows=18 width=8) (actual time=1245.570..1861.785 rows=80 loops=1)
  Workers Planned: 1
  Workers Launched: 1
  ->  Hash Join  (cost=44965.78..104831.61 rows=11 width=8) (actual time=1195.533..1816.966 rows=40 loops=2)
        Hash Cond: (a.type_id = a_data.id)
        ->  Hash Join  (cost=44964.54..104830.21 rows=11 width=10) (actual time=1195.073..1816.468 rows=40 loops=2)
              Hash Cond: (b.a_id = a.id)
              ->  Hash Join  (cost=44955.84..101733.27 rows=1176471 width=4) (actual time=927.335..1704.266 rows=1000000 loops=2)
                    Hash Cond: (b.type_id = b_data.id)
                    ->  Parallel Hash Join  (cost=44954.59..85555.55 rows=1176471 width=6) (actual time=927.243..1442.125 rows=1000000 loops=2)
                          Hash Cond: (c.b_id = b.id)
                          ->  Parallel Seq Scan on c  (cost=0.00..22575.71 rows=1176471 width=8) (actual time=0.060..185.123 rows=1000000 loops=2)
                          ->  Parallel Hash  (cost=24503.71..24503.71 rows=1176471 width=10) (actual time=486.106..486.106 rows=1000000 loops=2)
                                Buckets: 524288  Batches: 8  Memory Usage: 15872kB
                                ->  Parallel Seq Scan on b  (cost=0.00..24503.71 rows=1176471 width=10) (actual time=0.082..219.887 rows=1000000 loops=2)
                    ->  Hash  (cost=1.11..1.11 rows=11 width=2) (actual time=0.057..0.057 rows=11 loops=2)
                          Buckets: 1024  Batches: 1  Memory Usage: 9kB
                          ->  Seq Scan on type_data b_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.022..0.032 rows=11 loops=2)
              ->  Hash  (cost=8.58..8.58 rows=9 width=10) (actual time=0.111..0.111 rows=9 loops=2)
                    Buckets: 1024  Batches: 1  Memory Usage: 9kB
                    ->  Index Only Scan using cover_a on a  (cost=0.42..8.58 rows=9 width=10) (actual time=0.068..0.081 rows=9 loops=2)
                          Index Cond: (id < 10)
                          Heap Fetches: 18
        ->  Hash  (cost=1.11..1.11 rows=11 width=2) (actual time=0.077..0.078 rows=11 loops=2)
              Buckets: 1024  Batches: 1  Memory Usage: 9kB
              ->  Seq Scan on type_data a_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.043..0.052 rows=11 loops=2)
Planning Time: 2.215 ms
Execution Time: 1863.264 ms
*/

SET max_parallel_workers_per_gather = 2;
SET join_collapse_limit = 1;

EXPLAIN ANALYZE
SELECT a.id
FROM c
INNER JOIN b ON b.id = c.b_id
INNER JOIN type_data b_data ON b_data.id = b.type_id
INNER JOIN a ON a.id = b.a_id
INNER JOIN type_data a_data ON a_data.id = a.type_id
WHERE a.id < 10;
/*
Gather  (cost=36570.19..82131.77 rows=18 width=8) (actual time=916.016..1363.478 rows=80 loops=1)
  Workers Planned: 2
  Workers Launched: 2
  ->  Hash Join  (cost=35570.19..81129.97 rows=8 width=8) (actual time=938.827..1329.476 rows=27 loops=3)
        Hash Cond: (a.type_id = a_data.id)
        ->  Hash Join  (cost=35568.94..81128.61 rows=8 width=10) (actual time=938.741..1329.362 rows=27 loops=3)
              Hash Cond: (b.a_id = a.id)
              ->  Hash Join  (cost=35560.24..78932.40 rows=833333 width=4) (actual time=671.204..1245.548 rows=666667 loops=3)
                    Hash Cond: (b.type_id = b_data.id)
                    ->  Parallel Hash Join  (cost=35559.00..67472.83 rows=833333 width=6) (actual time=671.184..1053.643 rows=666667 loops=3)
                          Hash Cond: (c.b_id = b.id)
                          ->  Parallel Seq Scan on c  (cost=0.00..19144.33 rows=833333 width=8) (actual time=0.086..133.874 rows=666667 loops=3)
                          ->  Parallel Hash  (cost=21072.33..21072.33 rows=833333 width=10) (actual time=348.729..348.729 rows=666667 loops=3)
                                Buckets: 524288  Batches: 8  Memory Usage: 15904kB
                                ->  Parallel Seq Scan on b  (cost=0.00..21072.33 rows=833333 width=10) (actual time=0.057..157.732 rows=666667 loops=3)
                    ->  Hash  (cost=1.11..1.11 rows=11 width=2) (actual time=0.010..0.010 rows=11 loops=3)
                          Buckets: 1024  Batches: 1  Memory Usage: 9kB
                          ->  Seq Scan on type_data b_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.002..0.005 rows=11 loops=3)
              ->  Hash  (cost=8.58..8.58 rows=9 width=10) (actual time=0.029..0.029 rows=9 loops=3)
                    Buckets: 1024  Batches: 1  Memory Usage: 9kB
                    ->  Index Only Scan using cover_a on a  (cost=0.42..8.58 rows=9 width=10) (actual time=0.021..0.025 rows=9 loops=3)
                          Index Cond: (id < 10)
                          Heap Fetches: 27
        ->  Hash  (cost=1.11..1.11 rows=11 width=2) (actual time=0.020..0.020 rows=11 loops=3)
              Buckets: 1024  Batches: 1  Memory Usage: 9kB
              ->  Seq Scan on type_data a_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.011..0.014 rows=11 loops=3)
Planning Time: 1.031 ms
Execution Time: 1363.594 ms
*/

SET max_parallel_workers_per_gather = 4;
SET join_collapse_limit = 1;

EXPLAIN ANALYZE
SELECT a.id
FROM c
INNER JOIN b ON b.id = c.b_id
INNER JOIN type_data b_data ON b_data.id = b.type_id
INNER JOIN a ON a.id = b.a_id
INNER JOIN type_data a_data ON a_data.id = a.type_id
WHERE a.id < 10;
/*
Gather  (cost=31417.32..69132.88 rows=18 width=8) (actual time=855.339..1066.848 rows=80 loops=1)
  Workers Planned: 3
  Workers Launched: 3
  ->  Hash Join  (cost=30417.32..68131.08 rows=6 width=8) (actual time=745.537..1030.499 rows=20 loops=4)
        Hash Cond: (a.type_id = a_data.id)
        ->  Hash Join  (cost=30416.07..68129.75 rows=6 width=10) (actual time=745.225..1030.162 rows=20 loops=4)
              Hash Cond: (b.a_id = a.id)
              ->  Hash Join  (cost=30407.37..66427.50 rows=645161 width=4) (actual time=556.481..968.167 rows=500000 loops=4)
                    Hash Cond: (b.type_id = b_data.id)
                    ->  Parallel Hash Join  (cost=30406.13..57555.29 rows=645161 width=6) (actual time=556.424..824.914 rows=500000 loops=4)
                          Hash Cond: (c.b_id = b.id)
                          ->  Parallel Seq Scan on c  (cost=0.00..17262.61 rows=645161 width=8) (actual time=0.047..103.771 rows=500000 loops=4)
                          ->  Parallel Hash  (cost=19190.61..19190.61 rows=645161 width=10) (actual time=270.937..270.938 rows=500000 loops=4)
                                Buckets: 524288  Batches: 8  Memory Usage: 15936kB
                                ->  Parallel Seq Scan on b  (cost=0.00..19190.61 rows=645161 width=10) (actual time=0.093..122.527 rows=500000 loops=4)
                    ->  Hash  (cost=1.11..1.11 rows=11 width=2) (actual time=0.033..0.033 rows=11 loops=4)
                          Buckets: 1024  Batches: 1  Memory Usage: 9kB
                          ->  Seq Scan on type_data b_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.007..0.019 rows=11 loops=4)
              ->  Hash  (cost=8.58..8.58 rows=9 width=10) (actual time=0.089..0.090 rows=9 loops=4)
                    Buckets: 1024  Batches: 1  Memory Usage: 9kB
                    ->  Index Only Scan using cover_a on a  (cost=0.42..8.58 rows=9 width=10) (actual time=0.063..0.082 rows=9 loops=4)
                          Index Cond: (id < 10)
                          Heap Fetches: 36
        ->  Hash  (cost=1.11..1.11 rows=11 width=2) (actual time=0.051..0.051 rows=11 loops=4)
              Buckets: 1024  Batches: 1  Memory Usage: 9kB
              ->  Seq Scan on type_data a_data  (cost=0.00..1.11 rows=11 width=2) (actual time=0.029..0.033 rows=11 loops=4)
Planning Time: 0.800 ms
Execution Time: 1071.833 ms
*/


/*
-----------------------------------------------
Covering index
----------------------------------------------- 
*/

EXPLAIN ANALYSE
SELECT a.id
FROM a
WHERE a.id < 100;
/*
Index Only Scan using a_pkey on a  (cost=0.42..10.09 rows=95 width=8) (actual time=0.027..0.080 rows=99 loops=1)
  Index Cond: (id < 100)
  Heap Fetches: 99
Planning Time: 0.083 ms
Execution Time: 0.123 ms
*/

EXPLAIN ANALYSE
SELECT a.id, a.type_id
FROM a
WHERE a.id < 100;
/*
Index Scan using a_pkey on a  (cost=0.42..10.09 rows=95 width=10) (actual time=0.020..0.093 rows=99 loops=1)
  Index Cond: (id < 100)
Planning Time: 0.169 ms
Execution Time: 0.137 ms
*/

CREATE INDEX cover_a ON a(id) INCLUDE (type_id);

EXPLAIN ANALYSE
SELECT a.id, a.type_id
FROM a
WHERE a.id < 100;
/*
Index Only Scan using cover_a on a  (cost=0.42..10.09 rows=95 width=10) (actual time=0.028..0.073 rows=99 loops=1)
  Index Cond: (id < 100)
  Heap Fetches: 99
Planning Time: 0.394 ms
Execution Time: 0.126 ms
*/

EXPLAIN ANALYSE
SELECT a.id, a.type_id
FROM a
WHERE a.id = 1 AND a.type_id = 2;
/*
Index Only Scan using cover_a on a  (cost=0.42..8.45 rows=1 width=10) (actual time=0.052..0.052 rows=0 loops=1)
  Index Cond: (id = 1)
  Filter: (type_id = 2)
  Rows Removed by Filter: 1
  Heap Fetches: 1
Planning Time: 0.240 ms
Execution Time: 0.078 ms
*/